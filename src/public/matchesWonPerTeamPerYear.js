let teams=[]
let years=[]
function fetchData(){
    fetch('./matchesWonPerTeamPerYear.json')
    .then(obj=>obj.json())
    .then((json)=>{
        let obj=json['matchesWonPerTeamPerYear']
        for(let year in obj){
            for(let team in obj[year]){
                if(!teams.includes(team)){
                    teams.push(team)
                }
            }
        }
        teams=teams.sort()
        for(let year in obj){
            for(let team of teams){
                if(!(team in obj[year])){
                    obj[year][team]=0
                }
            }
        }
        for(let year in obj){
            years.push(year)
            obj[year]=Object.entries(obj[year])
        }
        return obj   
    })
    .then((obj)=>{
        highCharts(obj)
    })
    .catch((err)=>{
        console.error('error while fetching data',err)
    })     
}

function highCharts(arr){
    Highcharts.chart('matchesWonPerTeamPerYear', {
        chart: {
            type: 'spline'
        },
        title: {
            text: 'Number_of_matches_won_per_team_per_year'
        },
        subtitle: {
            text: 'Ipl'
        },
        xAxis: {
            categories:teams,
            title: {
                text: 'Teams'
            },
            min:0
        },
        yAxis: {
            title: {
                text: 'number of won matches'
            },
            min: 0
        },
        colors: ['#6CF', '#39F', '#06C', '#036', '#000','#ff4000','#80ff00','#ffbf00','#4000ff'],
        series: [{
            name: years[0],
            data: arr[years[0]].sort()
        },
        {
            name: years[1],
            data: arr[years[1]].sort()
        },
        {
            name: years[2],
            data: arr[years[2]].sort()
        },
        {
            name: years[3],
            data: arr[years[3]].sort()
        },
        {
            name: years[4],
            data: arr[years[4]].sort()
        }, 
        {
            name: years[5],
            data: arr[years[5]].sort()
        },
        {
            name: years[6],
            data: arr[years[6]].sort()
        },
        {
            name: years[7],
            data: arr[years[7]].sort()
        },
        {
            name: years[8],
            data: arr[years[8]].sort()
        },
        {
            name: years[9],
            data: arr[years[9]].sort()
        }],   
    })    
}

fetchData()