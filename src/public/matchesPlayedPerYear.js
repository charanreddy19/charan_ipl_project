let matchesArr=[]
function fetchData(){
    fetch('./matchesPerYear.json')
    .then(obj=>obj.json())
    .then((json)=>{
        let matchesObj=json.matchedplayedperyear
        for(let i in matchesObj){
            matchesArr.push([i,matchesObj[i]])
        }
        return matchesArr
    })
    .then(highCharts)
    .catch((err)=>{
        console.error('error while fetching matchesPerYear.json',err)
    })
}

function highCharts(arr){
    Highcharts.chart('matchesPerYear', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Matches-played-per-year'
        },
        subtitle: {
            text: 'IPL'
        },
        xAxis: {
            type: 'category',
            title: {
                text: 'Years'
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Matches'
            }
        },
        series: [{
            name: 'matches_played_per_year',
            data: arr,
        }]
    });
}

fetchData()