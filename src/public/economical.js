let economicalArr=[]
function fetchData(){
    fetch('./top10EconomicalBowlersInTheYear2015.json')
    .then(obj=>obj.json())
    .then((json)=>{
        let obj=json['top10EconomicalBowlersInYear2015']
        for(let bowler in obj){
            economicalArr.push([bowler,Number(obj[bowler])])
        }
        return economicalArr
    })
    .then(highCharts)
    .catch((err)=>{
        console.error('error while fetching data',err)
    })  
}

function highCharts(arr){
    Highcharts.chart('economical', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Top10EconomicalBowlersInTheYear2015'
        },
        subtitle: {
            text: 'IPL'
        },
        xAxis: {
            type: 'category',
            title: {
                text: 'bowler'
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'economy'
            }
        },
        series: [{
            name: 'top economical bowler ',
            data: arr,
        }]
    });
}

fetchData()