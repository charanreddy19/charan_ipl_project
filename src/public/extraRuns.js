let extraRuns=[]
function fetchData(){
    fetch('./extraRunsConcededPerTeamInYear2016.json')
    .then(obj=>obj.json())
    .then((json)=>{
        let obj=json['extraRunsConcededPerTeamInIpl2016']
        for(let team in obj){
            extraRuns.push([team,obj[team]])
        }
        console.log(extraRuns)
        return extraRuns
    })
    .then(highCharts)
    .catch((err)=>{
        console.error('error while fetching data',err)
    }) 
}

function highCharts(arr){
    Highcharts.chart('extraRuns', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Extra-runs-conceded-perTeam in 2016'
        },
        subtitle: {
            text: 'IPL'
        },
        xAxis: {
            type: 'category',
            title: {
                text: 'Teams'
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Extra_runs'
            }
        },
        series: [{
            name: 'visualization',
            data: arr,
        }]
    });
}

fetchData()