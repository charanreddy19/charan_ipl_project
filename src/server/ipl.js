const http=require('http')
const fs=require('fs')
const path=require('path')
function read(path){
    return new Promise((resolve,reject)=>{
        fs.readFile(path,'utf8', (err ,data) => {
            if (err) {
                reject(err)
            }
            else {
                resolve(data)  
            }
        })
    }) 
}

http.createServer((req,res)=>{
    const url = req.url
    res.on('error',(err)=>{
        console.log(err.message)
    })
    switch (url) {
        case '/' : {
            read(path.join(__dirname,'..','public/index.html'))
            .then((data)=>{
                res.writeHeader(200, {"Content-Type": "text/html"})
                res.write(data)
                res.end()
            })
            .catch((err)=>{
                console.log(err)
            })
            break
        }
        case '/matchesperyear' : {
            read(path.join(__dirname,'..','public/matchesPlayedPerYear.html'))
            .then((data)=>{
                res.writeHead(200,{'Content-Type': "text/html"})
                res.write(data)
                res.end()
            })
            .catch((err)=>{
                console.log(console.log(err))
            })   
            break
        }
        case '/matchesPlayedPerYear.js' : {
            read(path.join(__dirname,'..','public/matchesPlayedPerYear.js'))
            .then((data)=>{
                res.writeHeader(200, {"Content-Type": "text/javascript"})
                res.write(data)
                res.end()
            })
            .catch((err)=>{
                console.log(err)
            })
            break
        }
        case '/matchesPerYear.json' : {
            read(path.join(__dirname,'..','public/output/matchesPerYear.json'))
            .then((data)=>{
                res.writeHeader(200, {"Content-Type": "json/application"})
                res.write(data)
                res.end()
            })
            .catch((err)=>{
                console.log(err)
            }) 
            break
        }
        case '/matcheswonperteamperyear' : {
            read(path.join(__dirname,'..','public/matchesWonPerTeamPerYear.html'))
            .then((data)=>{
                res.writeHeader(200, {"Content-Type": "text/html"})
                res.write(data)
                res.end()
            })
            .catch((err)=>{
                console.log(err)
            })
            break
        }
        case '/matchesWonPerTeamPerYear.js' : {
            read(path.join(__dirname,'..','public/matchesWonPerTeamPerYear.js'))
            .then((data)=>{
                res.writeHeader(200, {"Content-Type": "text/javascript"})
                res.write(data);
                res.end()
            })
            .catch((err)=>{
                console.log(err)
            })
            break
        }
        case '/matchesWonPerTeamPerYear.json' : {
            read(path.join(__dirname,'..','public/output/matchesWonPerTeamPerYear.json'))
            .then((data)=>{
                res.writeHeader(200, {"Content-Type": "json/application"})
                res.write(data)
                res.end()
            })
            .catch((err)=>{
                console.log(err)
            })
            break
        }
        case '/extraruns' : {
            read(path.join(__dirname,'..','public/extraRuns.html'))
            .then((data)=>{
                res.writeHeader(200, {"Content-Type": "text/html"})
                res.write(data)
                res.end()
            })
            .catch((err)=>{
                console.log(err)
            })
            break
        }
        case '/extraRuns.js' : {
            read(path.join(__dirname,'..','public/extraRuns.js'))
            .then((data)=>{
                res.writeHeader(200, {"Content-Type": "text/javascript"})
                res.write(data)
                res.end()
            })
            .catch((err)=>{
                console.log(err)
            })
            break
        }
        case '/extraRunsConcededPerTeamInYear2016.json' : {
            read(path.join(__dirname,'..','public/output/extraRunsConcededPerTeamInYear2016.json'))
            .then((data)=>{
                res.writeHeader(200, {"Content-Type": "json/application"})
                res.write(data)
                res.end()
            })
            .catch((err)=>{
                console.log(err)
            })
            break
        }
        case '/economical' : {
            read(path.join(__dirname,'..','public/economical.html'))
            .then((data)=>{
                res.writeHeader(200, {"Content-Type": "text/html"})
                res.write(data)
                res.end()
            })
            .catch((err)=>{
                console.log(err)
            })
            break
        }
        case '/economical.js' : {
            read(path.join(__dirname,'..','public/economical.js'))
            .then((data)=>{
                res.writeHeader(200, {"Content-Type": "text/javascript"})
                res.write(data)
                res.end()
            })
            .catch((err)=>{
                console.log(err)
            })
            break
        }
        case '/top10EconomicalBowlersInTheYear2015.json' : {
            read(path.join(__dirname,'..','public/output/top10EconomicalBowlersInTheYear2015.json'))
            .then((data)=>{
                res.writeHeader(200, {"Content-Type": "json/application"})
                res.write(data)
                res.end()
            })
            .catch((err)=>{
                console.log(err)
            })
            break
        }
    }
}).listen(8000)