function wonMatches(obj){
    let matchesWonPerTeamPerYear={}
    obj.map(match=> {
        const winner=match.winner
        const season=match.season
        if(!(season in matchesWonPerTeamPerYear)){
            matchesWonPerTeamPerYear[season]={}
        }
        if(winner in matchesWonPerTeamPerYear[season]){
            matchesWonPerTeamPerYear[season][winner]+=1
        }
        else{
            matchesWonPerTeamPerYear[season][winner]=1
        }
    })
    return {'matchesWonPerTeamPerYear':matchesWonPerTeamPerYear}
}

module.exports.wonMatches=wonMatches