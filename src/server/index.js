const  csvtojson=require('csvtojson')
const fs=require('fs')
const path=require('path')
const matches=require(path.join(__dirname,'matchesPlayedPerYear.js'))
const won=require(path.join(__dirname,'matchesWonPerTeamPerYear.js'))
const extra=require(path.join(__dirname,'extraRunsConcededPerTeamInTheYear2016.js'))
const top=require(path.join(__dirname,'top10EconomicalBowlersInTheYear2015.js'))


function converter(path){
    return new Promise((resolve,reject)=>{
        csvtojson().fromFile(path)
        .then((jsonObj)=>{
            return resolve(jsonObj)
        })
        .catch((error)=>{
            return reject('csv to json convert error',err)
        })           
    })    
}

function write(path,obj){
    fs.writeFile(path,JSON.stringify(obj),(err)=>{
        if(err){
            console.log(err)
        }
    })
}


converter(path.join(__dirname,'..','data/matches.csv'))
.then((json)=>{
    let result=matches.matchesPerYear(json)
    let result1=won.wonMatches(json)
    write(path.join(__dirname,'..','public/output/matchesPerYear.json'),result)
    write(path.join(__dirname,'..','public/output/matchesWonPerTeamPerYear.json'),result1)
    converter(path.join(__dirname,'..','data/deliveries.csv'))
    .then((json1)=>{
    let result3=extra.extra(json,json1)
    let result4=top.economical(json,json1)
    write(path.join(__dirname,'..','public/output/extraRunsConcededPerTeamInYear2016.json'),result3)
    write(path.join(__dirname,'..','/public/output/top10EconomicalBowlersInTheYear2015.json'),result4)
    })
    .catch((err)=>{
    console.log(err)
    })  
})
.catch((err)=>{
    console.log(err)
})




