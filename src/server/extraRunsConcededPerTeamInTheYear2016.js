function extra(matches1,obj){
    let extraRuns={}
    matches=matches1.filter((match)=>{
        if(match.season==='2016'){
            return true
        }
    })
    matches.map(match=>{
        matchId=match.id
        obj.map(delivery => {
            deliveryId=delivery.match_id
            if(matchId===deliveryId){
                let extra_runs=delivery.extra_runs
                let bowling_team=delivery.bowling_team
                if(bowling_team in extraRuns){
                    extraRuns[bowling_team]=parseInt(extraRuns[bowling_team])+parseInt(extra_runs)
                }
                else{
                    extraRuns[bowling_team]=extra_runs
                }  
            }       
        })  
    })
    return {'extraRunsConcededPerTeamInIpl2016':extraRuns}
}

module.exports.extra=extra