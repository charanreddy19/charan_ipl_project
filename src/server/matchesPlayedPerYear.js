function matchesPerYear(obj){
    matchesPlayed={}
    obj.map(match=> {
        const season=match.season
        if(season in matchesPlayed){
            matchesPlayed[season]+=1
        }
        else{
            matchesPlayed[season]=1
        }
    });   
    return {'matchedplayedperyear':matchesPlayed}
}

module.exports.matchesPerYear=matchesPerYear
