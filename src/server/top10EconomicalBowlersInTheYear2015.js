function economical(matches1,obj){
    let economical={}
    let matches=matches1.filter((match)=>{
        if(match.season==='2015'){
            return true
        }
    })
    matches.map(match=>{
        matchId=match.id
        obj.map(delivery=> {
            deliveryId=delivery.match_id
            if(matchId===deliveryId){
                let bowler=delivery.bowler
                let bRuns=delivery.batsman_runs
                let wRuns=delivery.wide_runs
                let nRuns=delivery.noball_runs
                if(bowler in economical){
                    economical[bowler][0]=parseInt(economical[bowler][0])+parseInt(bRuns)+parseInt(wRuns)+parseInt(nRuns)
                    economical[bowler][1]=parseInt(economical[bowler][1])+1
                }
                else{
                    economical[bowler]=[parseInt(bRuns)+parseInt(wRuns)+parseInt(nRuns),1]
                }
            }
        })   
    })
    let economicalArr=[]
    for(let bowler in economical){
        // economy=(runs*6)/overs bowled
        economicalArr.push([bowler,((economical[bowler][0]*6)/(economical[bowler][1])).toFixed(2)])  
    }
    //bowler[0] contains name and bowler[1] contains economy
    economicalArr.sort(function(bowler1,bowler2){
        return bowler1[1]-bowler2[1]
    })
    let economicalObj=economicalArr.slice(0,10)
    let result={}
    economicalObj.forEach(element => {
        result[element[0]]=element[1]
    })
    return {'top10EconomicalBowlersInYear2015':result}
}
    
module.exports.economical=economical